.. _sec-development-interface-plugin:

``octoprint.plugin``
--------------------

.. automodule:: octoprint.plugin
   :members: plugin_manager, plugin_settings, call_plugin, PluginSettings
   :undoc-members:
